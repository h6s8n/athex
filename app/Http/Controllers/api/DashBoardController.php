<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashBoardController extends Controller
{
    public function dashboard(Request $request)
    {
        try {
            $userInfo = auth('api')->user();

            return response()->json([
                "status" => true,
                "messages" => ["به پنل کاربران خوش آمدید"],
                'name' => $userInfo->name,
                'family' => $userInfo->family,
                'email' => $userInfo->email,
                'nation_code' => $userInfo->nation_code,
                'mobile' => $userInfo->mobile,
            ]);

        } catch (\Exception $e) {
            return response()->json(["status" => false, "messages" => ["خطایی پیش آمده است"]], 402);
        }

    }

    //-----------------------------------------
    public function user_logout(Request $request)
    {
        if ($request->user()) {

            $request->user()->token()->revoke();
            return response()->json(["status" => true, "messages" => ["خروج از حساب کاربری با موفقیت انجام شد"]]);
        }

        return response()->json(["status" => false, "messages" => ["ورد به سیستم انجام نشده است"]]);

        // return $request->user()->token()->revoke();
//        if (Auth::check()) {
//            Auth::user()->AauthAcessToken()->delete();
//            return response()->json(["messages"=>["خروج از حساب کاربری با موفقیت انجام شد"]]);
//        }
//        return response()->json(["messages"=>["ورد به سیستم انجام نشده است"]]);
    }

    //-----------------------------------------------
    public function show_profile(Request $request)
    {
        try {

            $user = \auth("api")->user();
            return response()->json([
                "status" => true,
                $user
            ]);


        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "message" => "خطایی پیش آمده است"
            ]);
        }

    }

    //----------------------------------------------------------------
    public function rulesForupdate_profile($id)
    {
        return [

            'name' =>   ['required',"persian_alpha",'max:30', 'min:3'],
            'family' => ['required',"persian_alpha", 'max:40', 'min:3'],
            'nation_code' => ['required', 'min:10', 'max:10',"ir_national_code","unique:users,id,$id"],
            'mobile' => ['required', "unique:users,id,$id", 'max:11',"ir_mobile" ],

           "email"=>     ["required","email"],
           "birth_date"=>["required","shamsi_date"],
           "phone"=>     ["required","ir_phone_with_code"],
           "father"=>    ["required","persian_alpha"],
           "city"=>      ["required","persian_alpha","max:240"],
           "address"=>   ["required","persian_alpha","max:1000"],
           "zip_code"=>  ["required","ir_postal_code"],
           "account_number"=>["required",],
           "card_number"=>["required","ir_bank_card_number"],
           "sheba"=>      ["required","ir_sheba"],

           "national_card"=>     ["required","file","mimes:jpeg,png,pdf,jpg,max:3072",],
           "national_card_back"=>["required","file","mimes:jpeg,png,pdf,jpg,max:3072",],
           "birth_certificate"=> ["required","file","mimes:jpeg,png,pdf,jpg,max:3072",],
           "commitment"=>        ["required","file","mimes:jpeg,png,pdf,jpg,max:3072",],


        ];
    }
    //----------------------------------------------------------------

    public function update_profile(Request $request)
    {
        try {


            $user = \auth("api")->user();

            $validator = \Validator::make($request->all(), $this->rulesForupdate_profile($user->id));

            if ($validator->fails())

                return response()->json(['status' => false,
                    'messages' => $validator->getMessageBag()->toArray()],402);


            /**
             * check if profile confirmed by admin
             */
            if (isset($user) && $user != null && $user->account_confirm == false) {//update pr0file


                $user->name = $request->name;
                $user->family = $request->family;
                $user->email = $request->email;
                $user->nation_code = $request->nation_code;
                $user->mobile = $request->mobile;
                $user->phone = $request->phone;
                $user->birth_date = $request->birth_date;
                $user->father = $request->father;
                $user->city = $request->city;
                $user->zip_code = $request->zip_code;
                $user->address = $request->address;
                $user->account_number = $request->account_number;
                $user->card_number = $request->card_number;
                $user->sheba = $request->sheba;
                $user->account_confirm = false;


                /**
                 * recive document and update
                 */
                $profile_location = env("profile_location");

                if ($request->hasFile('national_card')) {

                    $time = time();
                    $ext = $request->file('national_card')->extension();
                    $filename = md5(uniqid("", true)) . '.' . $ext;
                    $filepath = $profile_location . $filename;
                    $request->national_card->move($profile_location, $filename);

                    $user->national_card = $filename;

                }

                if ($request->hasFile('national_card_back')) {

                    $time = time();
                    $ext = $request->file('national_card_back')->extension();
                    $filename = md5(uniqid("", true)) . '.' . $ext;
                    $filepath = $profile_location . $filename;
                    $request->national_card_back->move($profile_location, $filename);

                    $user->national_card_back = $filename;

                }

                if ($request->hasFile('birth_certificate')) {

                    $time = time();
                    $ext = $request->file('birth_certificate')->extension();
                    $filename = md5(uniqid("", true)) . '.' . $ext;
                    $filepath = $profile_location . $filename;
                    $request->birth_certificate->move($profile_location, $filename);

                    $user->birth_certificate = $filename;

                }
                if ($request->hasFile('commitment')) {

                    $time = time();
                    $ext = $request->file('commitment')->extension();
                    $filename = md5(uniqid("", true)) . '.' . $ext;
                    $filepath = $profile_location . $filename;
                    $request->commitment->move($profile_location, $filename);

                    $user->commitment = $filename;

                }

                if ($user->update())
                    return \response()->json(["status" => true,
                        "message" => "بروز رسانی پروفایل با موفقیت انجام شد"]);
                else
                    return \response()->json(["status" => false,
                        "message" => "خطایی پیش آمده است...!!!"]);


            } elseif (isset($user) && $user != null && $user->account_confirm == true) {

                return response()->json(["status" => false,
                    "message" => "حساب کاربری تایید شده و قابل بروز رسانی نمی باشد...!!!"
                ]);

            }


        } catch (\Exception $e) {

            return response()->json(["status" => false,
//                "message" => "خطایی پیش آمده است"
                "message" => $e->getMessage()
            ]);

        }

    }
    //-------------------------------------------------------------


}
