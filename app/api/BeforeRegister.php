<?php

namespace App\api;

use Illuminate\Database\Eloquent\Model;

class BeforeRegister extends Model
{
    protected $table="before_registers";
}
