<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreteBeforeRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('before_registers', function (Blueprint $table) {

            $table->bigInteger("id")->autoIncrement();
            $table->string('name',250)->nullable();
            $table->string('family',250)->nullable();
            $table->string("nation_code",50)->nullable();
            $table->string("mobile",20)->unique();
            $table->string("code",10);
            $table->boolean("confirm")->nullable()->default(0);
            $table->string("time",250);
            $table->integer("count");
            $table->enum("state",['block', 'regular']);
            $table->timestamps();
           $table->engine    = 'InnoDB';
           $table->charset   = 'utf8';
           $table->collation = 'utf8_bin';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('before_registers');
    }
}
