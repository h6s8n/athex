<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("user_login", "ProfileController@user_login")->name('user_login');
Route::post("user_auth", "ProfileController@user_auth")->name('user_auth');
Route::get("login", "ProfileController@user_login")->name("login");

Route::get("test" ,  "api\BeforeRegisterController@test")->name('test');
Route::get('/', function () {
    return public_path();
//    return view('welcome');
});
