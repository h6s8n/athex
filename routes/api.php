<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post("user_register" , "api\BeforeRegisterController@user_register")->name("user_register");
Route::post("code_confirm" ,  "api\BeforeRegisterController@code_confirm")->name('code_confirm');
Route::post("save_password" , "api\BeforeRegisterController@save_password")->name('save_password');

Route::post("user_login" , "api\BeforeRegisterController@user_login")->name('user_login');

Route::middleware('auth:api')->post('/user_logout',"api\DashBoardController@user_logout");
Route::middleware('auth:api')->post('/dashboard',"api\DashBoardController@dashboard");
Route::middleware('auth:api')->post('/show_profile',"api\DashBoardController@show_profile");
Route::middleware('auth:api')->post('/update_profile',"api\DashBoardController@update_profile");

/**
 * below route is for test - remove after test
 */

//Route::post("user_logout" , "api\DashBoardController@user_logout")->name('user_logout');
//Route::post("user_auth" , "ProfileController@user_auth")->name('user_auth');


//Route::get("test" ,  "api\BeforeRegisterController@test")->name('test');
