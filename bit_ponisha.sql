-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 01, 2021 at 08:24 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bit_ponisha`
--

-- --------------------------------------------------------

--
-- Table structure for table `before_registers`
--

CREATE TABLE `before_registers` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `family` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `nation_code` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_bin NOT NULL,
  `code` varchar(10) COLLATE utf8_bin NOT NULL,
  `confirm` tinyint(1) DEFAULT NULL,
  `time` varchar(250) COLLATE utf8_bin NOT NULL,
  `count` int(11) NOT NULL,
  `state` enum('block','regular') COLLATE utf8_bin NOT NULL DEFAULT 'regular',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(5, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(7, '2016_06_01_000004_create_oauth_clients_table', 2),
(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(12, '2014_10_12_000000_create_users_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('19da85a806689328bad5dcc07fa6509ba6767775eaf82645ebec6c56732fcf8a2ee4c92a42327859', 3, 1, 'create', '[]', 0, '2020-12-21 05:59:42', '2020-12-21 05:59:42', '2021-12-21 09:29:42'),
('26fc0bf7654defa7076a516979d8f71f14daae81b96e47033cc2e29d154ff689cd8dfa30ef281e37', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:47', '2020-12-21 05:36:47', '2021-12-21 09:06:47'),
('36c4e6e53c68bb59c101dbfd9c50daf8b0ffb5c2725e1569a9d867b211c9736555e8ef4f320c4acb', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:40', '2020-12-21 05:36:40', '2021-12-21 09:06:40'),
('6c200a2e549b4f17696bf65c357367de4a5b97b65b07bc2542002547832047a4aecd067815fb6fb1', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:50', '2020-12-21 05:36:50', '2021-12-21 09:06:50'),
('73156226c50db227921fb4fad6dfedd0c5768760ec8b7f2127ea5daa02fba1c297f4484c6e2e9fab', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:46', '2020-12-21 05:36:46', '2021-12-21 09:06:46'),
('8b404fc1d001278fe65c87f5f01e65cd11e10bb665ea432ac503d17c68e7f1713936eb5d840d9d16', 1, 3, 'create', '[]', 0, '2021-04-01 02:16:01', '2021-04-01 02:16:01', '2022-04-01 06:46:01'),
('8cacc2551740917e1b26f5bd94cc33f568c14a1d4acd34cb4df617b2fa2b7ea0d978145edbb62ddf', 2, 1, 'create', '[]', 0, '2020-12-21 05:38:48', '2020-12-21 05:38:48', '2021-12-21 09:08:48'),
('8f508d12670dd2767f470b4df1eccdf566edba8466fbdb9cf8b0d6514b458f42a231b7722d19d150', 2, 1, 'Token', '[]', 0, '2020-12-21 05:35:45', '2020-12-21 05:35:45', '2021-12-21 09:05:45'),
('93a030189ed7c91e432c7d08720f07cc4e617c8446d36c39eed5182ba2273bd69e34f3c39b4dc16a', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:20', '2020-12-21 05:36:20', '2021-12-21 09:06:20'),
('c2549f54fb76e860bfe7015106323fbae076e0ab4eb90f0f051eeb30d60b2bb4fd8d85bf2b7f4e41', 2, 1, 'Token', '[]', 0, '2020-12-21 05:35:56', '2020-12-21 05:35:56', '2021-12-21 09:05:56'),
('d7846e6835021d72a4edab1feb74c77d0bef93b04cffb2c967661e4b87606c4bec80d04280a6ac46', 2, 1, 'Token', '[]', 0, '2020-12-21 05:36:48', '2020-12-21 05:36:48', '2021-12-21 09:06:48'),
('e9d3f0edc514ffb30d745b738c3385f6330588eb6b101fa66e69ff162dba492dc3a2038aa0d8bd5d', 1, 1, 'LaravelAuthApp', '[]', 0, '2020-12-20 23:12:45', '2020-12-20 23:12:45', '2021-12-21 02:42:45'),
('ea3a9d42af37ac6824e9f54f328a60a22807d63c76a1f0e86e0633903ba8ad1ae2e17e750d5cb01d', 2, 1, 'create', '[]', 0, '2020-12-21 05:37:36', '2020-12-21 05:37:36', '2021-12-21 09:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '7E2RLNonB8ON3qFxzSxalp8HlXxAp2qLViyxOMKv', NULL, 'http://localhost', 1, 0, 0, '2020-12-20 22:56:37', '2020-12-20 22:56:37'),
(2, NULL, 'Laravel Password Grant Client', 'lexVkFgLbFRi7fAy7FllfRBz51dXLDUxzBCJtkQ8', 'users', 'http://localhost', 0, 1, 0, '2020-12-20 22:56:37', '2020-12-20 22:56:37'),
(3, NULL, 'Laravel Personal Access Client', 'JQgxxf2p6fXdD79gngHSrdiS06NKCOGhjdKfaQ7a', NULL, 'http://localhost', 1, 0, 0, '2021-04-01 02:13:29', '2021-04-01 02:13:29'),
(4, NULL, 'Laravel Password Grant Client', '1kfxJ5qyIUZzPLSIYgS2FfQl7EvaA7chgfXUdSJ9', 'users', 'http://localhost', 0, 1, 0, '2021-04-01 02:13:29', '2021-04-01 02:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-20 22:56:37', '2020-12-20 22:56:37'),
(2, 3, '2021-04-01 02:13:29', '2021-04-01 02:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nation_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(240) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_card` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `national_card_back` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_certificate` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commitment` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sheba` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_confirm` tinyint(1) DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `family`, `email`, `nation_code`, `mobile`, `email_verified_at`, `password`, `phone`, `birth_date`, `father`, `city`, `address`, `zip_code`, `national_card`, `national_card_back`, `birth_certificate`, `commitment`, `account_number`, `card_number`, `sheba`, `account_confirm`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'مرتضی', 'کاظمی', 'kazemimorteza68@gmail.com', '2620042607', '09941257504', NULL, '$2y$10$PEXuIi5chXUG.zKQZ6xxJerZD.PiFgoGdnPv9nGFIE8p1JSfTIZWS', '01344669689', '1369/12/5', 'رحمت', 'ماسال', 'گیلان ماسال  شهرک ولی عصر', '4381146146', 'eed77ed4c2fc1cf37d2dc92f3bcc1f1d.png', '844137de9b302f832fb1b0aed2f9ffc9.png', '5b8785b12a10f5f54e9c8e96994c2d22.png', '7f751786d36eb094a68614a575789984.png', '98745625877777', '6274129005473742', 'IR062960000000100324200001', 0, NULL, '2021-03-31 16:21:07', '2021-04-01 13:52:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `before_registers`
--
ALTER TABLE `before_registers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_mobile` (`mobile`) USING BTREE;

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nation_code_unique` (`nation_code`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `before_registers`
--
ALTER TABLE `before_registers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
