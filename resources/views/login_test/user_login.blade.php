<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Signin</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset("bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">


    <meta name="theme-color" content="#563d7c">

    {{--    <!-- Custom styles for this template --> signin.css--}}
    {{--    <link href="{{asset("bootstrap/signin.css")}}" rel="stylesheet">--}}
</head>
<body class="text-center" style="direction: rtl">
<div class="container-fluid" style="margin-top: 90px;margin-bottom: 30px">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4    border rounded border-secondary p-4" style="background-color:#fff;">
            <h6 class="text-center">ورود به سیستم</h6>

            <hr class="bg-danger">

{{--                @csrf--}}
                <input type="text" name="nation_code" id="nation_code"
                       placeholder="نام کاربری"
                       class="form-control"
                       maxlength="40"
                >
                <br>
                <div class="text-danger">
                    @if($errors->has('nation_code'))

                        {{$errors->first('nation_code')}}
                    @endif
                </div>
                <br>
                <div id="mobileAlert" class="text-danger bg-warning p-2 invisible rounded"></div>
                <input type="password" name="password" id="password"
                       class="form-control"
                       placeholder="کلمه عبور"
                       maxlength="30"
                >
                <br>
                <div class="text-danger">
                    @if($errors->has('password'))

                        {{$errors->first('password')}}
                    @endif
                </div>


                <br>
                <input type="hidden" name="recaptcha_v3" id="recaptcha_v3">
                @if((\Session::get("wrongLogin")))
                    <div class="row text-danger pt-2 pb-3 alert alert-danger">
                        {{\Session::get("wrongLogin")}}
                    </div>
                @endif

               <button class="btn btn-success pr-5 pl-5" onclick="loginTest()">ورود</button>



        </div>
        <div class="col-md-4"></div>
    </div>
</div>

<script type="text/javascript" src="{{asset("bootstrap/js/jquery-3.5.1.min.js")}}"></script>

<script type="text/javascript">




    function loginTest(){



        //------------------------------------------------
        // $.ajaxSetup({
        //     headers: {
        //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }
        // });
        $.ajax({

            url: "{{route('user_auth')}}",
            method: 'post',
            dataType: 'json',
            data: {

                userId:        $("#nation_code").val(),
                password:      $("#password").val(),
                captcha_token: $("#recaptcha_v3").val(),


            },
            beforeSend: function () {
                console.log(this.data)
            }
            ,
            success: function (result) {

                if (result['success']) {

                    console.log("success",result);


                } else if (result['error']) {

                    console.log(result['error']);

                    $.each(result['error'], function (index, data) {

                        switch (index) {

                            case "name": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#nameError").append(data[i]);
                                break;
                            }
                            case "email": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#emailError").append(data[i]);
                                break;
                            }
                            case "mobile": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#mobileError").append(data[i]);
                                break;
                            }
                            case "role": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#roleError").append(data[i]);
                                break;
                            }
                            /*case "newsLetter": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#newsLetterError").append(data[i]);
                                break;
                            }*/
                            case "nation_code": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#nationCodeError").append(data[i]);
                                break;
                            }
                            case "password": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#passwordError").append(data[i]);
                                break;
                            }
                            case "reNewPassword": {
                                for (var i = 0; i < data.length; ++i)
                                    $("#reNewPasswordError").append(data[i]);
                                break;
                            }


                        }


                    });


                    $("#saveSpinner").removeClass("fa-spinner");


                }
            },  //success
            error: function (data) {

                alert("خطایی رخ داده است");

                $("#saveSpinner").removeClass("fa-spinner");


            }//error

        }) //$.ajax

        //-----------------------------------------------
        return false;

    }





</script>

<script src="https://www.google.com/recaptcha/api.js?render={{env('G_RECAPTCHA_SITE_KEY')}}"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute("{{env('G_RECAPTCHA_SITE_KEY')}}", {action: 'user_login'}).then(function(token) {
            if(token) {
                //js
                document.getElementById('recaptcha_v3').value = token;
                //if you use jquery library
                //$("#recaptcha_v3").val(token);
                console.log(token)
            }
        });
    });
</script>
<style>
    .grecaptcha-badge {
        visibility: hidden;
    }
</style>
</body>
</html>
