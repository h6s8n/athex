<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();
            $table->string('name',100);
            $table->string('family',200);
            $table->string('email')->unique()->nullable();
            $table->string('nation_code',30)->unique();
            $table->string('mobile',30)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            /**
             * second part
             */
            $table->string('phone',50)->nullable();
            $table->string('birth_date',50)->nullable();
            $table->string('father',200)->nullable();
            $table->string('city',240)->nullable();
            $table->longText('address')->nullable();
            $table->string('zip_code',50)->nullable();
            $table->string('national_card',150)->nullable();
            $table->string('national_card_back',150)->nullable();
            $table->string('birth_certificate',150)->nullable();
            $table->string('commitment',150)->nullable();
            $table->string('account_number',50)->nullable();
            $table->string('card_number',50)->nullable();
            $table->string('sheba',60)->nullable();
            $table->boolean('account_confirm')->default(false)->nullable();




            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
