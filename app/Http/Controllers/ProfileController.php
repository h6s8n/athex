<?php
/*
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
}*/


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule; //import Rule class
class ProfileController extends Controller
{
    public function user_login($_back = null)
    {


        return view("login_test.user_login");
    }
    //..............................................................

    /**
     * Rules for user authenticate and login
     * @return array
     */
    public function rulesForUserLogin()
    {
        return [

            'userId' => ['required',
                'regex:/^[0-9]+$/',
                'max:11',
                'min:10',
            ],
            'password' => ['required', 'max:30', 'min:6',
                //'regex:/^[a-z0-9\+_\-. : ]+$/'
                'regex:/^[a-zA-Z0-9\.\+\-\_\$()\#\@\!{}\%]+$/'
            ],




        ];
    }

    public function user_auth(Request $request)
    {
        try {

            $customMessages = [

                'userId.regex' => "فرمت نام کاربری نادرست می باشد",
                'userId.min' => "حداقل طول نام کاربری 11 کاراکتر می باشد",
                'userId.max' => "حداکثر طول نام کاربری 40 کاراکتر می باشد",
                'password.min' => "حداقل طول پسورد ۶ کاراکتر می باشد",
                'password.max' => "حداکثر طول پسورد ۳۰ کاراکتر می باشد",

            ];


            $vars = array(
                'secret' => env('G_RECAPTCHA_SECRET_KEY'),
                "response" => $request->input('captcha_token')
            );
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
            $encoded_response = curl_exec($ch);
            $response = json_decode($encoded_response, true);
            curl_close($ch);

            if($response['success'] && $response['action'] == 'user_auth' && $response['score']>0.5) {

                // dd($request->toArray());
                $validator = \Validator::make(["userId"=>$request->userId,"password"=>$request->password], $this->rulesForUserLogin());

                if ($validator->fails()) {

                      return response()->json(['error' => $validator->getMessageBag()->toArray()]);
                    //  dd($validator->getMessageBag()->toArray());
                  //  return redirect(route("user_login"))->withErrors($validator)->withInput();
                    // return redirect(url("manage/createPost"))->withErrors($validator)->withInput();
                }

                if (Auth::attempt(['nation_code' => $request->userId, 'password' => $request->password])) {

                    $user = Auth::user();

                    return response()->json(['success' => "success login"]);
                }
                // dd("here");


            } else {

                return response()->json(['error' =>"invalid request by captcha"]);

            }







        } catch (\Exceptin $e) {
            return response()->json(['error' => $e->getMessage()]);
            // return abort(404);
        }
    }
    //..............................................................
    public function log_out()
    {
        Auth::logout();
        return redirect("login");
    }
    //..............................................................



}

