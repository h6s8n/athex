<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\api\BeforeRegister;

use App\User;

use Illuminate\Support\Facades\Auth;
use phpseclib\Crypt\Hash;
use Kavenegar;

class BeforeRegisterController extends Controller
{

    public function rulesForUserRegister()
    {
        return [
            'name' => ['required', 'max:30', 'min:3'],
            'family' => ['required', 'max:40', 'min:3'],
            'nation_code' => ['required', 'min:10', 'max:10', 'regex:/^[0-9]+$/', 'unique:users'],
            'mobile' => ['required', 'unique:users',
                'regex:/^(09)[0-9]{9}$/', 'max:11',
                // 'regex:/^(09|۰۹)[0-9\۰\۱\۲\۳\۴\۵\۶\۷\۸\۹]{9}$/',
            ],


        ];
    }

    //----------------------------------------------------
    public function user_register(Request $request)
    {

        try {


            $validator = \Validator::make($request->all(), $this->rulesForUserRegister());

            if ($validator->fails()) {

                return response()->json(['status' => false, 'messages' => $validator->getMessageBag()->toArray()],402);

            } else {//check history state and send sms


                $br = BeforeRegister::where(['mobile' => $request->mobile])->get();
                \Log::alert($br);
                if ($br->count() > 0 && $br[0]->count >= 3) { //blocked

                    if (time() - $br[0]->time >= 10800) { //unblock

                        $br[0]->count = 1;
                        $br[0]->time = time();
                        $code = rand(100000, 999999);
                        $br[0]->code = $code;
                        $br[0]->state = "regular";
                        $br[0]->update();

                        if (!$this->sendSMS($request->mobile, $code, "کد ورود به حساب کاربری "))
                            return response()->json(['status' => false,
                                'messages' => ['sms' => 'خطا در ارسال کد فعال سازی ! لطفا مجددا سعی نمایید']],402);
                        // return $fail('خطا در ارسال کد فعال سازی ! لطفا مجددا سعی نمایید');

                    } else {

                        $br[0]->time = time();
                        $br[0]->state = "block";
                        $br[0]->update();
                        return response()->json(['status' => false,
                            'messages' => ['sms' => 'این شماره به مدت سه ساعت توسط سیستم مسدود شد']],402);

                        // echo "Block for 3 hour";
                    }


                } else if ($br->count() > 0 && $br[0]->count < 3) {

                    $br[0]->count = $br[0]->count + 1;
                    $code = rand(100000, 999999);
                    $br[0]->code = $code;
                    $br[0]->state = "regular";
                    $br[0]->time = time();
                    $br[0]->update();
                    if (!$this->sendSMS($request->mobile, $code, "کد ورود به حساب کاربری "))
                        return response()->json(['status' => false,
                            'messages' => ['sms' => 'خطا در ارسال کد فعال سازی ! لطفا مجددا سعی نمایید']],402);

                } else if ($br->count() == 0) {

                    $br = new BeforeRegister();

                    $br->name = $request->name;
                    $br->family = $request->family;
                    $br->nation_code = $request->nation_code;

                    $br->mobile = $request->mobile;
                    $br->count = 1;
                    $code = rand(100000, 999999);
                    $br->code = $code;
                    $br->time = time();
                    $br->state = "regular";
                    $br->save();
                    if (!$this->sendSMS($request->mobile, $code, "کد ورود به حساب کاربری "))
                        return response()->json(['status' => false,
                            'messages' => ['sms' => 'خطا در ارسال کد فعال سازی ! لطفا مجددا سعی نمایید']],402);
                }
                // }//if user ==0


            }

            $br = BeforeRegister::where(['mobile' => $request->mobile])->get();
            $br[0]->name = $request->name;
            $br[0]->family = $request->family;
            $br[0]->nation_code = $request->nation_code;
            // $br[0]->confirm = true;
            $br[0]->update();

            return response()->json(['status' => true,
                "messages" => ["success" => "لطفا کد ارسال شده را وارد کنید"]], 200);


        } catch (\Exceptin $e) {

            return response()->json(['status' => false, "messages" => [$e->getMessage()]],402);

        }

    }

    public function code_confirm(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), $this->rulesForConfirm());

            if ($validator->fails()) {

                return response()->json(["status" => false, "messages" => $validator->getMessageBag()->toArray()],402);

            }


            $br = BeforeRegister::where(['mobile' => $request->mobile])->get();

            if ($br->count() && $br[0]->state == "regular" && ($br[0]->code == ($request->code)) &&
                (time() - $br[0]->time <= 180)) {//exist mobile and not blocked and confirm sms code
                // time<130second

                $br[0]->confirm = true;
                $br[0]->update();

                return \response()->json(["status" => true, 'messages' => ["code_msg" => "کد تایید صحیح می باشد"]], 200);

            } else if ($br->count() && $br[0]->state == "regular" &&
                ($br[0]->code == ($request->code))
                && (time() - $br[0]->time > 130)) {


                return \response()->json(["status" => false, 'messages' => ["code_msg" => "کد مورد نظر منقضی شده است"]],402);

            } else {


                return \response()->json(["status" => false, 'messages' => ["code_msg" => "کد وارد شده صحیح نمی باشد"]],402);

            }


        } catch (\Exception $e) {

            return \response()->json(["status" => false, 'messages' => ["code_msg" => $e->getMessage()]],402);
        }

    }
    //---------------------------------------------------
    public function rulesForConfirm()
    {
        return [
            'code' => ['required', 'regex:/^[0-9\۰\۱\۲\۳\۴\۵\۶\۷\۸\۹]{6,6}$/'],
            'mobile' => ['required', 'exists:before_registers', 'regex:/^(09)[0-9]{9}$/', 'max:11',]
        ];
    }
    //---------------------------------------------
    public function save_password(Request $request)
    {
        try {

            $validator = \Validator::make($request->all(), $this->rulesForPassword());

            if ($validator->fails()) {

                return response()->json(["status" => false, "messages" => $validator->getMessageBag()->toArray()],402);

            }


            $br = BeforeRegister::where(['mobile' => $request->mobile])->get();

            if ($br->count() && $br[0]->state == "regular" && ($br[0]->confirm) && (time() - $br[0]->time <= 300)) {//exist mobile and not blocked and confirm sms code time<130second


                $user = new User();
                $user->name = $br[0]->name ;
                $user->family = $br[0]->family;
                $user->mobile = $br[0]->mobile;
                $user->nation_code = $br[0]->nation_code;
                $user->password = \Illuminate\Support\Facades\Hash::make($request->password);
                $user->save();
                $br[0]->delete();

                return \response()->json(["status" => true, 'messages' =>
                    ["account_msg" => "حساب کاربری شما با موفقیت ایجاد شد"]], 200);

            } else if ($br->count() && $br[0]->state == "regular" && ($br[0]->confirm) && !(time() - $br[0]->time <=
                    300)) {
                $br[0]->delete();
                return \response()->json(["status" => false, 'messages' =>
                    ["account_msg" => "زمان ایجاد اکانت بیشتر از حد مجاز است لطفا مجددا برای ایجاد حساب کاربری اقدام
                    کنید"]],402);

            } else if ($br->count() && $br[0]->state == "regular" && (!$br[0]->confirm) && (time() - $br[0]->time <=
                    300)) {
                $br[0]->delete();
                return \response()->json(["status" => false, 'messages' =>
                    ["account_msg" => "شماره تلفن شما تایید نشده است لطفا مجددا اقدام به حساب کاربری نمایید"]],402);
            }


        } catch (\Exception $e) {

            return \response()->json(["status" => false, 'messages' => ["account_msg" => $e->getMessage()]],402);
        }

    }

    //---------------------------------------------------
    public function rulesForPassword()
    {
        return [
            'mobile' => ['required', 'exists:before_registers', 'regex:/^(09)[0-9]{9}$/', 'max:11',],
            'password' => ['required', 'max:30', 'min:6', 'same:rePassword', 'regex:/^[a-zA-Z0-9\.\+\-\_\$()\#\@\!{}\%]+$/'],//0k
            'rePassword' => ["nullable", 'required_with:password', 'max:30', 'min:6'],
        ];
    }

    //---------------------------------------------------
    public function rulesForUser_login()
    {
        return [
            'nation_code' => ['required', 'min:10', 'max:10', 'regex:/^[0-9]+$/'],
            'password' => ['required', 'max:30', 'min:6', 'regex:/^[a-zA-Z0-9\.\+\-\_\$()\#\@\!{}\%]+$/'],
        ];
    }

    //---------------------------------------------
    public function user_login(Request $request)
    {
        try {

            $vars = array(
                'secret' => env('G_RECAPTCHA_SECRET_KEY'),
                "response" => $request->input('captcha_token')
            );
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
            $encoded_response = curl_exec($ch);
            $response = json_decode($encoded_response, true);
            curl_close($ch);

            if ($response['success'] && $response['action'] == 'user_login' && $response['score'] > 0.5) {

                $validator = \Validator::make($request->all(), $this->rulesForUser_login());

                if ($validator->fails()) {

                    return response()->json(["status" => false, "messages" => $validator->getMessageBag()->toArray()],402);

                }
                if (Auth::attempt(['nation_code' => $request->nation_code, 'password' => $request->password])) {

                    $user = Auth::user();
                    $token = $user->createToken('create')->accessToken;
                    return \response()->json(["status" => true, "token" => $token]);

                } else {

                    return \response()->json(["status" => false, "messages" => "نام کاربری یا پسورد اشتباه است"],402);
                }


            } else {

                return response()->json(["status" => false, "messages" => "درخواست شما نا معتبر است-عدم تطبیق کد امنیتی"],402);

            }


            //-------------------------------------------------------------------------


        } catch (\Exception $e) {

            return \response()->json(["status" => false, 'messages' => ["account_msg" => $e->getMessage()]],402);
        }
    }

    //-----------------------------------------------------------------
    public function test()
    {


//        if (Auth::attempt(['nation_code' =>"2620042607", 'password' =>"123456"])) {
//
//            $user = Auth::user();
//            $token = \Auth::user()->createToken('Token')->accessToken;
//            //$token = $user->createToken('create')->accessToken;
//            return \response()->json(["status"=>true,"token"=>$token]);
//
//        }else{
//
//            return \response()->json(["status"=>false]);
//        }

        $this->sendSMS("09117661876", "123456", "message ");


    }


    public function sendSMS($receptor, $code, $message)
    {
        try {
            $api = new \Kavenegar\KavenegarApi("7136576F3256554C4C686336633543373768394263326B30746F6242313862766C2B35737044474D5A45593D");
            $sender = "10001000440400";
            // $message = "";
            // $receptor = array("09117661876","09123356784");
            $messageContent = $message . "  " . $code;
            $result = $api->Send($sender, $receptor, $messageContent);
            if ($result) {
//                 foreach($result as $r){
//                    echo "messageid = $r->messageid";
//                    echo "message = $r->message";
//                    echo "status = $r->status";
//                    echo "statustext = $r->statustext";
//                    echo "sender = $r->sender";
//                    echo "receptor = $r->receptor";
//                    echo "date = $r->date";
//                    echo "cost = $r->cost";
//                }
                return true;
            }
        } catch (\Kavenegar\Exceptions\ApiException $e) {
            // echo $e->errorMessage();
            return false;
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد

        } catch (\Kavenegar\Exceptions\HttpException $e) {
            // echo $e->errorMessage();
            return false;
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد

        }
    }


}
